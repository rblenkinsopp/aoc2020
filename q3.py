import io
import math

from util import AOCQuestion


class Q3(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> list[str]:
        return list(self.get_input_by_line(stream))

    @staticmethod
    def traverse(puzzle_input, right, down) -> str:
        width = len(puzzle_input[0])
        tree_map = ''.join(puzzle_input)

        x = 0
        y = 0
        index = 0

        while index < len(tree_map):
            yield tree_map[index] == "#"
            x += right
            y += down
            index = (x % width) + (y * width)

    def part1(self, puzzle_input) -> int:
        return sum(self.traverse(puzzle_input, 3, 1))

    def part2(self, puzzle_input, part1_answer: int = 0) -> int:
        return math.prod([sum(self.traverse(puzzle_input, right, down)) for right, down in [
            (1, 1),
            (3, 1),
            (5, 1),
            (7, 1),
            (1, 2),
        ]])


if __name__ == "__main__":
    Q3().solve()
