import io
import itertools
import math
from typing import Generator

from util import AOCQuestion, NoAnswerFoundError


class Q1(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> Generator[int, None, None]:
        return self.get_input_as_ints(stream)

    def part1(self, puzzle_input) -> int:
        for numbers in itertools.combinations(puzzle_input, 2):
            if sum(numbers) == 2020:
                return math.prod(numbers)
        raise NoAnswerFoundError

    def part2(self, puzzle_input, part1_answer: int = 0) -> int:
        for numbers in itertools.combinations(puzzle_input, 3):
            if sum(numbers) == 2020:
                return math.prod(numbers)
        raise NoAnswerFoundError


if __name__ == "__main__":
    Q1().solve()
