import io
import re
from typing import Generator

from util import AOCQuestion


class Q2(AOCQuestion):
    regex = re.compile(r"(?P<min>\d+)-(?P<max>\d+) (?P<letter>[a-z]): (?P<password>[a-z]+)")

    def parse_input(self, stream: io.TextIOBase) -> Generator[str, None, None]:
        return self.get_input_by_line(stream)

    @staticmethod
    def is_password_valid_part1(entry: str) -> bool:
        matches = Q2.regex.match(entry)

        min_occurrences = int(matches.group('min'))
        max_occurrences = int(matches.group('max'))
        letter = matches.group('letter')
        password = matches.group('password')
        occurrences = password.count(letter)

        return max_occurrences >= occurrences >= min_occurrences

    def part1(self, puzzle_input) -> int:
        return sum(map(self.is_password_valid_part1, puzzle_input))

    @staticmethod
    def is_password_valid_part2(entry: str) -> bool:
        matches = Q2.regex.match(entry)

        position1 = int(matches.group('min'))
        position2 = int(matches.group('max'))
        letter = matches.group('letter')
        password = matches.group('password')

        char1 = password[position1 - 1]
        char2 = password[position2 - 1]

        return char1 != char2 and (char1 == letter or char2 == letter)

    def part2(self, puzzle_input, part1_answer: int = 0) -> int:
        return sum(map(self.is_password_valid_part2, puzzle_input))


if __name__ == "__main__":
    Q2().solve()
