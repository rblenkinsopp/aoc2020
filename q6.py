import io
from typing import Generator

from util import AOCQuestion


class Q6(AOCQuestion):
    def parse_input(self, stream: io.TextIOBase) -> Generator[list[set[str]], None, None]:
        entry = list()
        for line in self.get_input_by_line(stream):
            if line == "":
                yield entry
                entry.clear()
            else:
                entry.append(set(line))
        yield entry

    def part1(self, puzzle_input: Generator[list[set[str]], None, None]) -> int:
        return sum([len(set.union(*p)) for p in puzzle_input])

    def part2(self, puzzle_input: Generator[list[set[str]], None, None], part1_answer: int = 0) -> int:
        return sum([len(set.intersection(*p)) for p in puzzle_input])


if __name__ == "__main__":
    Q6().solve()
