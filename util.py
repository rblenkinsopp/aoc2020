import io
from typing import Generator


class NoAnswerFoundError(Exception):
    """Exception raised when no valid answer was found"""
    pass


class GridData:
    def __init__(self, stream: io.TextIOBase):
        lines = list(stream)
        self.columns = len(lines[0])
        self.rows = len(lines)
        self.cells = list([list(line) for line in AOCQuestion.get_input_by_line(stream)])

    def all_cells(self) -> Generator[str, None, None]:
        for y in range(0, self.rows):
            for x in range(0, self.columns):
                yield self.cells[y][x]


class AOCQuestion:
    """Base class for Advent of Code (AOC) questions"""

    @staticmethod
    def get_input_by_line(stream: io.TextIOBase) -> Generator[str, None, None]:
        """
        Returns a list of lines from the input file as a generator.
        :param stream: the input text-stream to be iterated over.
        """
        for line in stream:
            yield line.strip()

    @staticmethod
    def get_input_as_ints(stream: io.TextIOBase) -> Generator[int, None, None]:
        """
        Returns a list of ints from the input file as a generator.
        :param stream: the input text-stream to be iterated over.
        """
        for line in AOCQuestion.get_input_by_line(stream):
            yield int(line)

    def get_question_number(self) -> int:
        """
        Retrieves the question number from the name of the question class
        :return: an int representing the question number
        """
        return int(self.__class__.__name__.lstrip("Q"))

    def get_input_file(self) -> str:
        """
        Retrieves the relevant input file name, ready to be opened.
        :return: the filename relevant to the question.
        """
        return "inputs/q{}-input.txt".format(self.get_question_number())

    def parse_input(self, stream: io.TextIOBase) -> Generator:
        raise NotImplementedError

    def part1(self, puzzle_input: [int]) -> int:
        """
        Solve part 1 of the question
        :param puzzle_input: the input the the puzzle (normally a list).
        :return: the answer to the puzzle as str
        """
        raise NotImplementedError

    def part2(self, puzzle_input: [int], part1_answer: int = 0) -> int:
        """
        Solve part 2 of the question
        :param puzzle_input: the input the the puzzle (normally a list).
        :param part1_answer: the ansewer from part1
        :return: the answer to the puzzle as str
        """
        raise NotImplementedError

    def solve(self) -> None:
        """
        Solves the question and prints out the answers to both parts.
        """
        question_number = self.__class__.__name__.lstrip("Q")

        # Solve part 1
        try:
            input1 = self.parse_input(open(self.get_input_file()))
            answer1 = self.part1(input1)
            print("Question {}, Part 1: `{}`".format(question_number, answer1))

            # Solve part 2
            try:
                input2 = self.parse_input(open(self.get_input_file()))
                answer2 = self.part2(input2, answer1)
                print("Question {}, Part 2: `{}`".format(question_number, answer2))
            except NotImplementedError:
                pass
        except NotImplementedError:
            pass
