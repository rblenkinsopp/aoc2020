import collections
import io
import itertools
from typing import Generator, Iterable

from util import AOCQuestion, NoAnswerFoundError


class Q9(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> Generator[int, None, None]:
        return self.get_input_as_ints(stream)

    @staticmethod
    def is_valid_next_number(numbers: Iterable[int], number) -> bool:
        return number in [sum(x) for x in itertools.combinations(numbers, 2)]

    @staticmethod
    def find_invalid_number(numbers: Iterable[int], preamble_len: int = 25) -> int:
        window = collections.deque(itertools.islice(numbers, preamble_len), preamble_len)

        for i in numbers:
            if Q9.is_valid_next_number(window, i):
                window.append(i)
            else:
                return i
        raise NoAnswerFoundError

    @staticmethod
    def find_encryption_weakness(numbers: Iterable[int], invalid_number: int) -> int:
        code = list(numbers)
        for i in range(0, len(code)):
            total = code[i]
            y = i + 1
            while total < invalid_number:
                total += code[y]
                if total == invalid_number:
                    valid_range = code[i:y+1]
                    return max(valid_range) + min(valid_range)
                y += 1
        raise NoAnswerFoundError

    def part1(self, puzzle_input: Iterable[int]) -> int:
        return self.find_invalid_number(puzzle_input, 25)

    def part2(self, puzzle_input: Iterable[int], part1_answer: int = 0) -> int:
        return self.find_encryption_weakness(puzzle_input, part1_answer)


if __name__ == "__main__":
    Q9().solve()
