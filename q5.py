import io
from typing import Generator

from util import AOCQuestion, NoAnswerFoundError


class Q5(AOCQuestion):
    rows = 128
    columns = 8

    def parse_input(self, stream: io.TextIOBase) -> Generator[dict[str, str], None, None]:
        for line in self.get_input_by_line(stream):
            yield list(line)

    def decode(self, entry: str) -> tuple[int, int, int]:
        row = range(0, self.rows)
        column = range(0, self.columns)

        for char in entry:
            if char == 'F':
                row = range(row.start, row.stop - (len(row) // 2))
            elif char == 'B':
                row = range(row.start + (len(row) // 2), row.stop)
            elif char == 'L':
                column = range(column.start, column.stop - (len(column) // 2))
            elif char == 'R':
                column = range(column.start + (len(column) // 2), column.stop)

        if len(row) == 1 and len(column) == 1:
            row = row[0]
            column = column[0]
        else:
            raise NoAnswerFoundError

        seat_id = (row * 8) + column
        return row, column, seat_id

    def part1(self, puzzle_input) -> int:
        return max(seat_id for _, _, seat_id in [self.decode(entry) for entry in puzzle_input])

    def part2(self, puzzle_input, part1_answer: int = 0) -> int:
        taken_seats = [x for _, _, x in sorted([self.decode(entry) for entry in puzzle_input])]
        all_seats = [x for x in range(taken_seats[0], taken_seats[-1])]
        my_seat = [x for x in all_seats if x not in taken_seats]

        if len(my_seat) != 1:
            raise NoAnswerFoundError

        return my_seat[0]


if __name__ == "__main__":
    Q5().solve()
