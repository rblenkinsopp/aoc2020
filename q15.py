import io
from typing import Generator

from util import AOCQuestion


class Q15(AOCQuestion):
    def parse_input(self, stream: io.TextIOBase) -> Generator[int, None, None]:
        puzzle_input = stream.read()
        for num in puzzle_input.split(","):
            yield int(num)

    @staticmethod
    def get_elf_numbers(puzzle_input: Generator[int, None, None], terminating_at: int) -> int:
        initial_numbers = list(puzzle_input)
        indexes = {n: i for i, n in enumerate(initial_numbers[:-1])}
        last_number = initial_numbers[-1]

        for index in range(len(initial_numbers) - 1, terminating_at- 1):
            last_index = indexes.get(last_number)
            indexes[last_number] = index
            last_number = (0 if last_index is None else index - last_index)
        return last_number

    def part1(self, puzzle_input: Generator[int, None, None]) -> int:
        return self.get_elf_numbers(puzzle_input, 2020)

    def part2(self, puzzle_input: Generator[int, None, None], part1_answer: int = 0) -> int:
        return self.get_elf_numbers(puzzle_input, 30000000)


if __name__ == "__main__":
    Q15().solve()
