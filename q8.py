import copy
import io
from dataclasses import dataclass

from util import AOCQuestion, NoAnswerFoundError


@dataclass
class Instruction:
    op: str
    arg: int
    executed: bool


class InfiniteLoopError(Exception):
    def __init__(self, pc: int, acc:int):
        self.pc = pc
        self.acc = acc


class Q8(AOCQuestion):
    operations = [
        'acc',
        'jmp',
        'nop',
    ]

    def parse_input(self, stream: io.TextIOBase) -> list[Instruction]:
        return [Instruction(x[0], int(x[1]), False) for x in (x.split(' ') for x in self.get_input_by_line(stream))]

    @staticmethod
    def execute_program(program: list[Instruction]) -> int:
        mem = program
        acc = 0
        pc = 0

        while pc < len(mem):
            i = mem[pc]

            # If we hit an instruction we have already seen, terminate and output ACC.
            if i.executed:
                raise InfiniteLoopError(pc, acc)

            if i.op == 'acc':
                acc += i.arg
                pc += 1
            elif i.op == 'jmp':
                pc += i.arg
            elif i.op == 'nop':
                pc += 1
            i.executed = True

        return acc

    def part1(self, puzzle_input: list[Instruction]) -> int:
        try:
            return self.execute_program(puzzle_input)
        except InfiniteLoopError as e:
            return e.acc
        except:
            raise NoAnswerFoundError

    def part2(self, puzzle_input: list[Instruction], part1_answer: int = 0) -> int:
        for i, instr in enumerate(puzzle_input):
            modified_program = copy.deepcopy(puzzle_input)
            if instr.op == 'jmp':
                modified_program[i].op = 'nop'
            elif instr.op == 'nop':
                modified_program[i].op = 'jmp'
            elif instr.op == 'acc':
                continue

            try:
                return self.execute_program(modified_program)
            except InfiniteLoopError:
                continue

        raise NoAnswerFoundError


if __name__ == "__main__":
    Q8().solve()
