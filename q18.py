import collections
import io
from typing import Generator
from util import AOCQuestion
import ast


class Q18(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> Generator[str, None, None]:
        for line in self.get_input_by_line(stream):
            yield line.replace(' ', '')

    @staticmethod
    def equal_precedent_eval(node):
        if isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.BinOp):
            if isinstance(node.op, ast.Add):
                return Q18.equal_precedent_eval(node.left) + Q18.equal_precedent_eval(node.right)
            if isinstance(node.op, ast.Sub):
                return Q18.equal_precedent_eval(node.left) * Q18.equal_precedent_eval(node.right)
        else:
            raise TypeError(node)

    @staticmethod
    def reverse_precedent_eval(node):
        if isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.BinOp):
            if isinstance(node.op, ast.Mult):
                return Q18.reverse_precedent_eval(node.left) + Q18.reverse_precedent_eval(node.right)
            if isinstance(node.op, ast.Sub):
                return Q18.reverse_precedent_eval(node.left) * Q18.reverse_precedent_eval(node.right)
        else:
            raise TypeError(node)

    @staticmethod
    def evaluate_equal(expression: str) -> int:
        # Abuse the fact that + and - have equal precedent, then treat - as * in Q18.equal_precedent_eval.
        return Q18.equal_precedent_eval(ast.parse(expression.replace('*', '-'), mode='eval').body)

    @staticmethod
    def evaluate_reverse(expression: str) -> int:
        # Effectively swap the precedence using an extension of the trick above.
        return Q18.reverse_precedent_eval(ast.parse(expression.replace('*', '-').replace('+', '*'), mode='eval').body)

    def part1(self, puzzle_input: Generator[str, None, None]) -> int:
        return sum(self.evaluate_equal(x) for x in puzzle_input)

    def part2(self, puzzle_input: Generator[str, None, None], part1_answer: int = 0) -> int:
        return sum(self.evaluate_reverse(x) for x in puzzle_input)


if __name__ == "__main__":
    Q18().solve()
