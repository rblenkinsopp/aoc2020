import io
import unittest

from q1 import Q1
from q10 import Q10
from q11 import Q11
from q12 import Q12
from q13 import Q13
from q14 import Q14
from q15 import Q15
from q16 import Q16
from q17 import Q17
from q18 import Q18
from q19 import Q19
from q2 import Q2
from q3 import Q3
from q4 import Q4
from q5 import Q5
from q6 import Q6
from q7 import Q7
from q8 import Q8
from q9 import Q9


class TestQ1(unittest.TestCase):
    sample = """1721
979
366
299
675
1456"""

    def test_part1(self):
        q1 = Q1()
        result = q1.part1(q1.parse_input(io.StringIO(self.sample)))
        self.assertEqual(514579, result)

    def test_part2(self):
        q1 = Q1()
        result = q1.part2(q1.parse_input(io.StringIO(self.sample)), 0)
        self.assertEqual(241861950, result)


class TestQ2(unittest.TestCase):
    sample = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc"""

    def test_part1(self):
        q2 = Q2()
        result = q2.part1(q2.parse_input(io.StringIO(self.sample)))
        self.assertEqual(2, result)

    def test_part2(self):
        q2 = Q2()
        result = q2.part2(q2.parse_input(io.StringIO(self.sample)), 0)
        self.assertEqual(1, result)


class TestQ3(unittest.TestCase):
    sample = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""

    def test_part1(self):
        q3 = Q3()
        result = q3.part1(q3.parse_input(io.StringIO(self.sample)))
        self.assertEqual(7, result)

    def test_part2(self):
        q3 = Q3()
        result = q3.part2(q3.parse_input(io.StringIO(self.sample)))
        self.assertEqual(336, result)


class TestQ4(unittest.TestCase):
    sample = """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""

    sample_invalid = """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"""

    sample_valid = """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"""

    def test_validate_byr(self):
        from q4 import validate_byr
        self.assertTrue(validate_byr("2002"))
        self.assertFalse(validate_byr("2003"))

    def test_validate_hgt(self):
        from q4 import validate_hgt
        self.assertTrue(validate_hgt("60in"))
        self.assertTrue(validate_hgt("190cm"))
        self.assertFalse(validate_hgt("190in"))
        self.assertFalse(validate_hgt("190"))

    def test_validate_hcl(self):
        from q4 import validate_hcl
        self.assertTrue(validate_hcl("#123abc"))
        self.assertFalse(validate_hcl("#123abz"))
        self.assertFalse(validate_hcl("123abc"))

    def test_validate_ecl(self):
        from q4 import validate_ecl
        self.assertTrue(validate_ecl("brn"))
        self.assertFalse(validate_ecl("wat"))

    def test_validate_pid(self):
        from q4 import validate_pid
        self.assertTrue(validate_pid("000000001"))
        self.assertFalse(validate_pid("0123456789"))

    def test_part1(self):
        q4 = Q4()
        result = q4.part1(q4.parse_input(io.StringIO(self.sample)))
        self.assertEqual(2, result)

    def test_part2(self):
        q4 = Q4()
        result1 = q4.part2(q4.parse_input(io.StringIO(self.sample_valid)))
        self.assertEqual(4, result1)
        result2 = q4.part2(q4.parse_input(io.StringIO(self.sample_invalid)))
        self.assertEqual(0, result2)


class TestQ5(unittest.TestCase):
    sample_entry = """abcx
abcy
abcz"""
    sample = """abc

a
b
c

ab
ac

a
a
a
a

b"""

    def test_decode(self):
        q5 = Q5()
        self.assertEqual((44, 5, 357), q5.decode("FBFBBFFRLR"))
        self.assertEqual((70, 7, 567), q5.decode("BFFFBBFRRR"))
        self.assertEqual((14, 7, 119), q5.decode("FFFBBBFRRR"))
        self.assertEqual((102, 4, 820), q5.decode("BBFFBBFRLL"))


class TestQ6(unittest.TestCase):
    sample_entry = """abcx
abcy
abcz"""
    sample = """abc

a
b
c

ab
ac

a
a
a
a

b"""

    def test_part1(self):
        q6 = Q6()
        result = q6.part1(q6.parse_input(io.StringIO(self.sample_entry)))
        self.assertEqual(result, 6)
        result = q6.part1(q6.parse_input(io.StringIO(self.sample)))
        self.assertEqual(result, 11)

    def test_part2(self):
        q6 = Q6()
        result = q6.part2(q6.parse_input(io.StringIO(self.sample_entry)))
        self.assertEqual(result, 3)
        result = q6.part2(q6.parse_input(io.StringIO(self.sample)))
        self.assertEqual(result, 6)


class TestQ7(unittest.TestCase):
    sample = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""

    sample2 = """shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""

    def test_part1(self):
        q7 = Q7()
        result = q7.part1(q7.parse_input(io.StringIO(self.sample)))
        self.assertEqual(4, result)

    def test_part2(self):
        q7 = Q7()
        result = q7.part2(q7.parse_input(io.StringIO(self.sample)))
        self.assertEqual(32, result)
        result = q7.part2(q7.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(126, result)


class TestQ8(unittest.TestCase):
    sample = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""

    def test_part1(self):
        q8 = Q8()
        result = q8.part1(q8.parse_input(io.StringIO(self.sample)))
        self.assertEqual(5, result)

    def test_part2(self):
        q8 = Q8()
        result = q8.part2(q8.parse_input(io.StringIO(self.sample)))
        self.assertEqual(8, result)


class TestQ9(unittest.TestCase):
    sample = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"""

    def test_is_valid_next_number(self):
        self.assertEqual(True, Q9.is_valid_next_number(range(1, 26), 26))
        self.assertEqual(True, Q9.is_valid_next_number(range(1, 26), 49))
        self.assertEqual(False, Q9.is_valid_next_number(range(1, 26), 100))
        self.assertEqual(False, Q9.is_valid_next_number(range(1, 26), 50))

    def test_find_invalid_number(self):
        q9 = Q9()
        self.assertEqual(127, q9.find_invalid_number(q9.parse_input(io.StringIO(self.sample)), 5))

    def test_find_encryption_weakness(self):
        q9 = Q9()
        self.assertEqual(62, q9.find_encryption_weakness(q9.parse_input(io.StringIO(self.sample)), 127))


class TestQ10(unittest.TestCase):
    sample1 = """16
10
15
5
1
11
7
19
6
12
4"""

    sample2 = """28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3"""

    def test_find_joltage_differences(self):
        q10 = Q10()
        result = Q10.find_joltage_differences(list(q10.parse_input(io.StringIO(self.sample1))))
        self.assertEqual(7, result[1])
        self.assertEqual(5, result[3])

        result = Q10.find_joltage_differences(list(q10.parse_input(io.StringIO(self.sample2))))
        self.assertEqual(22, result[1])
        self.assertEqual(10, result[3])

    def test_find_adapter_chain_combinations(self):
        q10 = Q10()
        result = Q10.find_adapter_chain_combinations(list(q10.parse_input(io.StringIO(self.sample1))))
        self.assertEqual(8, result)

        result = Q10.find_adapter_chain_combinations(list(q10.parse_input(io.StringIO(self.sample2))))
        self.assertEqual(19208, result)

    def test_part1(self):
        q10 = Q10()
        result = q10.part1(q10.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(7 * 5, result)
        result = q10.part1(q10.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(22 * 10, result)

    def test_part2(self):
        # q10 = Q10()
        # result = q10.part2(q10.parse_input(io.StringIO(self.sample)))
        # self.assertEqual(8, result)
        pass


class TestQ11(unittest.TestCase):
    sample = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"""

    case1 = """.......#.
...#.....
.#.......
.........
..#L....#
....#....
.........
#........
...#....."""
    case2 = """.............
.L.L.#.#.#.#.
............."""
    case3 = """.##.##.
#.#.#.#
##...##
...L...
##...##
#.#.#.#
.##.##."""

    def test_get_visible_seats(self):
        q11 = Q11()
        seat_map = q11.parse_input(io.StringIO(self.case1))
        result = q11.get_visible_seats(seat_map, 3, 4)
        self.assertEqual(8, result.count('#'))
        seat_map = q11.parse_input(io.StringIO(self.case2))
        result = q11.get_visible_seats(seat_map, 1, 1)
        self.assertEqual(1, result.count('L'))
        self.assertEqual(0, result.count('#'))
        seat_map = q11.parse_input(io.StringIO(self.case3))
        result = q11.get_visible_seats(seat_map, 3, 3)
        self.assertEqual(0, result.count('#'))

    def test_part1(self):
        q11 = Q11()
        result = q11.part1(q11.parse_input(io.StringIO(self.sample)))
        self.assertEqual(37, result)

    def test_part2(self):
        q11 = Q11()
        result = q11.part2(q11.parse_input(io.StringIO(self.sample)))
        self.assertEqual(26, result)


class TestQ12(unittest.TestCase):
    sample = """F10
N3
F7
R90
F11"""

    def test_part1(self):
        q12 = Q12()
        result = q12.part1(q12.parse_input(io.StringIO(self.sample)))
        self.assertEqual(25, result)

    def test_part2(self):
        q12 = Q12()
        result = q12.part2(q12.parse_input(io.StringIO(self.sample)))
        self.assertEqual(286, result)


class TestQ13(unittest.TestCase):
    sample = """939
7,13,x,x,59,x,31,19"""
    case1 = """0
17,x,13,19"""
    case2 = """0
67,7,59,61"""
    case3 = """0
67,x,7,59,61"""
    case4 = """0
67,7,x,59,61"""
    case5 = """0
1789,37,47,1889"""

    def test_part1(self):
        q13 = Q13()
        result = q13.part1(q13.parse_input(io.StringIO(self.sample)))
        self.assertEqual(295, result)

    def test_part2(self):
        q13 = Q13()
        result = q13.part2(q13.parse_input(io.StringIO(self.sample)))
        self.assertEqual(1068781, result)
        result = q13.part2(q13.parse_input(io.StringIO(self.case1)))
        self.assertEqual(3417, result)
        result = q13.part2(q13.parse_input(io.StringIO(self.case2)))
        self.assertEqual(754018, result)
        result = q13.part2(q13.parse_input(io.StringIO(self.case3)))
        self.assertEqual(779210, result)
        result = q13.part2(q13.parse_input(io.StringIO(self.case4)))
        self.assertEqual(1261476, result)
        result = q13.part2(q13.parse_input(io.StringIO(self.case5)))
        self.assertEqual(1202161486, result)


class TestQ14(unittest.TestCase):
    sample1 = """mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0"""
    sample2 = """mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"""

    def test_part1(self):
        q14 = Q14()
        result = q14.part1(q14.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(165, result)

    def test_part2(self):
        q14 = Q14()
        result = q14.part2(q14.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(208, result)


class TestQ15(unittest.TestCase):
    sample1 = """0,3,6"""
    sample2 = """1,3,2"""
    sample3 = """2,1,3"""
    sample4 = """1,2,3"""
    sample5 = """2,3,1"""
    sample6 = """3,2,1"""
    sample7 = """3,1,2"""

    def test_part1(self):
        q15 = Q15()
        result = q15.part1(q15.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(436, result)

    @unittest.skip("Q15-2: Runs too slowly to be enabled by default")
    def test_part2(self):
        q15 = Q15()
        result = q15.part2(q15.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(175594, result)
        result = q15.part2(q15.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(2578, result)
        result = q15.part2(q15.parse_input(io.StringIO(self.sample3)))
        self.assertEqual(3544142, result)
        result = q15.part2(q15.parse_input(io.StringIO(self.sample4)))
        self.assertEqual(261214, result)
        result = q15.part2(q15.parse_input(io.StringIO(self.sample5)))
        self.assertEqual(6895259, result)
        result = q15.part2(q15.parse_input(io.StringIO(self.sample6)))
        self.assertEqual(18, result)
        result = q15.part2(q15.parse_input(io.StringIO(self.sample7)))
        self.assertEqual(362, result)


class TestQ16(unittest.TestCase):
    sample1 = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12"""
    sample2 = """class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9"""

    def test_part1(self):
        q16 = Q16()
        result = q16.part1(q16.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(71, result)

    def test_part2(self):
        q16 = Q16()
        result = q16.part2(q16.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(1, result)


class TestQ17(unittest.TestCase):
    sample1 = """.#.
..#
###"""

    def test_part1(self):
        q17 = Q17()
        result = q17.part1(q17.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(112, result)

    @unittest.skip("Q17-2: Runs too slowly to be enabled by default")
    def test_part2(self):
        q17 = Q17()
        result = q17.part2(q17.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(848, result)


class TestQ18(unittest.TestCase):
    sample0 = """1 + 2 * 3 + 4 * 5 + 6"""
    sample1 = """1 + (2 * 3) + (4 * (5 + 6))"""
    sample2 = """2 * 3 + (4 * 5)"""
    sample3 = """5 + (8 * 3 + 9 + 3 * 4 * 3)"""
    sample4 = """5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"""
    sample5 = """((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"""

    def test_part1(self):
        q18 = Q18()
        result = q18.part1(q18.parse_input(io.StringIO(self.sample0)))
        self.assertEqual(71, result)
        result = q18.part1(q18.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(51, result)
        result = q18.part1(q18.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(26, result)
        result = q18.part1(q18.parse_input(io.StringIO(self.sample3)))
        self.assertEqual(437, result)
        result = q18.part1(q18.parse_input(io.StringIO(self.sample4)))
        self.assertEqual(12240, result)
        result = q18.part1(q18.parse_input(io.StringIO(self.sample5)))
        self.assertEqual(13632, result)

    def test_part2(self):
        q18 = Q18()
        result = q18.part2(q18.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(51, result)
        result = q18.part2(q18.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(46, result)
        result = q18.part2(q18.parse_input(io.StringIO(self.sample3)))
        self.assertEqual(1445, result)
        result = q18.part2(q18.parse_input(io.StringIO(self.sample4)))
        self.assertEqual(669060, result)
        result = q18.part2(q18.parse_input(io.StringIO(self.sample5)))
        self.assertEqual(23340, result)


class TestQ19(unittest.TestCase):
    sample1 = """0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb"""
    sample2 = """42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"""

    def test_part1(self):
        q19 = Q19()
        result = q19.part1(q19.parse_input(io.StringIO(self.sample1)))
        self.assertEqual(2, result)

    def test_part2(self):
        q19 = Q19()
        result = q19.part1(q19.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(3, result)
        result = q19.part2(q19.parse_input(io.StringIO(self.sample2)))
        self.assertEqual(12, result)

