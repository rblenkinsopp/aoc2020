import collections
import io
import regex
from typing import Generator, Union

from util import AOCQuestion


class Q19(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> tuple[dict[int, list[Union[int, str]]], list[str]]:
        rules = dict()
        for line in self.get_input_by_line(stream):
            if line == '':
                break
            parts = line.split(':', 1)
            rules[int(parts[0])] = [int(x) if x.isnumeric() else x.strip('"') for x in parts[1].strip().split(' ')]

        messages = [line for line in self.get_input_by_line(stream)]
        return rules, messages

    @staticmethod
    def resolve_rule(rules: dict[int, list[Union[int, str]]], rule_number: int) -> str:
        result = str()
        rule_parts = rules[rule_number]

        if len(rule_parts) == 1 and isinstance(rule_parts[0], str):
            return str(rule_parts[0])
        else:
            for x in rules[rule_number]:
                if isinstance(x, str):
                    result += str(x)
                elif rule_number == x:
                    # Recursive -
                    result += r'(?&r{})'.format(rule_number)
                else:
                    result += Q19.resolve_rule(rules, x)
            if rule_number == 0:
                return '^' + result + '$'
            else:
                return '(?P<r{}>'.format(rule_number) + result + ')'

    def part1(self, puzzle_input: tuple[dict[int, list[Union[int, str]]], list[str]]) -> int:
        # Combine rules into a regex.
        rules, messages = puzzle_input
        rules_string = self.resolve_rule(rules, 0)
        pattern = regex.compile(rules_string)
        return len(list(filter(pattern.match, messages)))

    def part2(self, puzzle_input: tuple[dict[int, list[Union[int, str]]], list[str]], part1_answer: int = 0) -> int:
        rules, messages = puzzle_input

        # Alter rules 8 and 11
        rules[8] = [42, '|', 42, 8]
        rules[11] = [42, 31, '|', 42, 11, 31]

        rules_string = self.resolve_rule(rules, 0)
        pattern = regex.compile(rules_string)
        return len(list(filter(pattern.match, messages)))


if __name__ == "__main__":
    Q19().solve()
