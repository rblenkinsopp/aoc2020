import io
import re

from util import AOCQuestion


class Q7(AOCQuestion):
    rule_regex = re.compile(r"^(?P<key>\w+\s\w+) bags contain (?P<contains>.+).$")
    entry_regex = re.compile(r"^(?P<num>\d+)\s(?P<colour>\w+\s\w+) bags?$")

    def parse_input(self, stream: io.TextIOBase) -> dict[str, list[tuple[int, str]]]:
        rules = {}
        for line in self.get_input_by_line(stream):
            matches = self.rule_regex.match(line)
            key = matches.group('key')
            contains = matches.group('contains').split(', ')

            entries = [(int(m.group('num')), m.group('colour')) for m in [self.entry_regex.match(c) for c in contains] if m is not None]
            rules[key] = entries if entries is not None else []
        return rules

    def can_contain(self, rules: dict[str, list[tuple[int, str]]], bag_colour: str) -> set[str]:
        possible_holding_colours = set()
        for k, v in rules.items():
            for x, y in v:
                if y == bag_colour:
                    possible_holding_colours.add(k)
                    [possible_holding_colours.add(i) for i in self.can_contain(rules, k)]
        return possible_holding_colours

    def calculate_sub_bags(self, rules: dict[str, list[tuple[int, str]]], bag_colour: str) -> int:
        return sum(amount + (amount * self.calculate_sub_bags(rules, colour)) for amount, colour in rules[bag_colour])

    def part1(self, puzzle_input: dict[str, list[tuple[int, str]]]) -> int:
        return len(self.can_contain(puzzle_input, 'shiny gold'))

    def part2(self, puzzle_input: dict[str, list[tuple[int, str]]], part1_answer: int = 0) -> int:
        return self.calculate_sub_bags(puzzle_input, 'shiny gold')


if __name__ == "__main__":
    Q7().solve()
