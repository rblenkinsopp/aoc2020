import io
import itertools
import math
import re

from util import AOCQuestion, NoAnswerFoundError


class Q16(AOCQuestion):
    def parse_input(self, stream: io.TextIOBase) -> tuple[dict[str, set[int]], list[int], list[list[int]]]:
        regex = re.compile(r"^(.+): (\d+)-(\d+) or (\d+)-(\d+)$")
        rules = dict()
        my_ticket = list()
        nearby_tickets = list()
        section = 0

        for line in self.get_input_by_line(stream):
            if line == "":
                continue
            elif line == "your ticket:":
                section = 1
                continue
            elif line == "nearby tickets:":
                section = 2
                continue

            if section == 0:  # Rules
                match = regex.match(line)
                rule, x1, x2, y1, y2 = match.groups()
                rules[rule] = {x for x in itertools.chain(range(int(x1), int(x2) + 1), range(int(y1), int(y2) + 1))}
            elif section == 1:  # My Ticket
                my_ticket = [int(x) for x in line.split(',')]
            elif section == 2:  # Nearby Tickets
                nearby_tickets.append([int(x) for x in line.split(',')])

        return rules, my_ticket, nearby_tickets

    @staticmethod
    def is_ticket_valid(rules: dict[str, set[int]], ticket: list[int]) -> tuple[bool, list[int]]:
        valid_values = set.union(*rules.values())
        invalid_values = []
        for field in ticket:
            if field not in valid_values:
                invalid_values.append(field)
        return len(invalid_values) == 0, invalid_values

    def part1(self, puzzle_input: tuple[dict[str, set[int]], list[int], list[list[int]]]) -> int:
        rules, my_ticket, nearby_tickets = puzzle_input

        invalid_values = []
        for ticket in nearby_tickets:
            valid, invalid_field_values = self.is_ticket_valid(rules, ticket)
            invalid_values.extend(invalid_field_values)

        return sum(invalid_values)

    def part2(self, puzzle_input: tuple[dict[str, set[int]], list[int], list[list[int]]], part1_answer: int = 0) -> int:
        rules, my_ticket, nearby_tickets = puzzle_input
        valid_tickets = [t for t in nearby_tickets if self.is_ticket_valid(rules, t)[0]]

        # For each column, find a rule which satisfies all conditions.
        possible_rules = []
        for i, _ in enumerate(my_ticket):
            possible_rules.append([r for r, v in rules.items() if all(t[i] in v for t in valid_tickets)])

        # Resolve which column must be which.
        used_fields = set()
        for r in sorted(possible_rules, key=len):
            [r.remove(x) for x in used_fields]
            if len(r) != 1:
                raise NoAnswerFoundError
            used_fields.add(r[0])

        # Map this back and lookup the fields.
        return math.prod(v for k, v in {field: my_ticket[i] for i, field in enumerate([v[0] for v in possible_rules])}.items() if k.startswith('departure'))


if __name__ == "__main__":
    Q16().solve()
