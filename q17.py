import io
import itertools
import math
import re
from collections import defaultdict
from copy import deepcopy

from util import AOCQuestion, NoAnswerFoundError


class Space3D:
    def __init__(self, z: int, y: int, x: int):
        self.z_bounds = range(0, z)
        self.y_bounds = range(0, y)
        self.x_bounds = range(0, x)
        self.active_cells = set()

    def __setitem__(self, key: tuple[int, int, int], active: bool) -> None:
        if active:
            self.active_cells.add(key)
        else:
            self.active_cells.discard(key)

    def __getitem__(self, key: tuple[int, int, int]) -> bool:
        return key in self.active_cells

    def __len__(self) -> int:
        return len(self.active_cells)

    def __str__(self) -> str:
        output = str()

        output += 'zrange = {}'.format(self.z_range()) + '\n'
        output += 'yrange = {}'.format(self.y_range()) + '\n'
        output += 'xrange = {}'.format(self.x_range()) + '\n\n'

        for z in self.z_range():
            output += 'z={}'.format(z) + '\n'
            for y in self.y_range():
                output += ''.join(['#' if self[(z, y, x)] else '.' for x in self.x_range()]) + '\n'
            output += '\n'
        return output

    def count_active(self) -> int:
        return len(self)

    def z_range(self) -> range:
        return self.z_bounds

    def y_range(self) -> range:
        return self.y_bounds

    def x_range(self) -> range:
        return self.x_bounds

    def process_cycle(self) -> None:
        original = deepcopy(self)
        self.z_bounds = range(self.z_bounds.start - 1, self.z_bounds.stop + 1)
        self.y_bounds = range(self.y_bounds.start - 1, self.y_bounds.stop + 1)
        self.x_bounds = range(self.x_bounds.start - 1, self.x_bounds.stop + 1)

        # Iterate through each cell, find the neighbours and apply the rules
        for z in self.z_range():
            for y in self.y_range():
                for x in self.x_range():
                    cube_active = self[(z, y, x)]
                    active_neighbours = original._count_active_neighbours((z, y, x))
                    if cube_active:
                        self[(z, y, x)] = active_neighbours in [2, 3]
                    else:  # cube is inactive
                        self[(z, y, x)] = active_neighbours == 3

    def _count_active_neighbours(self, key: tuple[int, int, int]) -> int:
        z, y, x = key
        count = 0
        for zi in range(z - 1, z + 2):
            for yi in range(y - 1, y + 2):
                for xi in range(x - 1, x + 2):
                    if xi == x and yi == y and zi == z:
                        continue
                    elif self[(zi, yi, xi)]:
                        count += 1
        return count


class Space4D(Space3D):
    def __init__(self, w: int, z: int, y: int, x: int):
        super().__init__(z, y, x)
        self.w_bounds = range(0, w)

    def __setitem__(self, key: tuple[int, int, int, int], active: bool) -> None:
        if active:
            self.active_cells.add(key)
        else:
            self.active_cells.discard(key)

    def __getitem__(self, key: tuple[int, int, int, int]) -> bool:
        return key in self.active_cells

    def __len__(self) -> int:
        return len(self.active_cells)

    def __str__(self) -> str:
        output = str()

        output += 'wrange = {}'.format(self.z_range()) + '\n'
        output += 'zrange = {}'.format(self.z_range()) + '\n'
        output += 'yrange = {}'.format(self.y_range()) + '\n'
        output += 'xrange = {}'.format(self.x_range()) + '\n\n'

        for w in self.w_range():
            for z in self.z_range():
                output += 'z={}, w={}'.format(z, w) + '\n'
                for y in self.y_range():
                    output += ''.join(['#' if self[(w, z, y, x)] else '.' for x in self.x_range()]) + '\n'
                output += '\n'
            return output

    def count_active(self) -> int:
        return len(self)

    def w_range(self) -> range:
        return self.w_bounds

    def process_cycle(self) -> None:
        original = deepcopy(self)
        self.w_bounds = range(self.w_bounds.start - 1, self.w_bounds.stop + 1)
        self.z_bounds = range(self.z_bounds.start - 1, self.z_bounds.stop + 1)
        self.y_bounds = range(self.y_bounds.start - 1, self.y_bounds.stop + 1)
        self.x_bounds = range(self.x_bounds.start - 1, self.x_bounds.stop + 1)

        # Iterate through each cell, find the neighbours and apply the rules
        for w in self.z_range():
            for z in self.z_range():
                for y in self.y_range():
                    for x in self.x_range():
                        cube_active = self[(w, z, y, x)]
                        active_neighbours = original._count_active_neighbours((w, z, y, x))
                        if cube_active:
                            self[(w, z, y, x)] = active_neighbours in [2, 3]
                        else:  # cube is inactive
                            self[(w, z, y, x)] = active_neighbours == 3

    def _count_active_neighbours(self, key: tuple[int, int, int, int]) -> int:
        w, z, y, x = key
        count = 0
        considered = 0
        for wi in range(w - 1, w + 2):
            for zi in range(z - 1, z + 2):
                for yi in range(y - 1, y + 2):
                    for xi in range(x - 1, x + 2):
                        if xi == x and yi == y and zi == z and wi == w:
                            continue
                        elif self[(wi, zi, yi, xi)]:
                            count += 1
                        considered += 1
        return count


class Q17(AOCQuestion):
    def parse_input(self, stream: io.TextIOBase) -> list[str]:
        return list(self.get_input_by_line(stream))

    def part1(self, puzzle_input: list[str]) -> int:
        lines = puzzle_input
        rows = len(lines)
        columns = len(lines[0])
        space = Space3D(1, rows, columns)
        for y, line in enumerate(lines):
            for x, cell in enumerate(line):
                space[(0, y, x)] = (cell == '#')

        for i in range(0, 6):
            space.process_cycle()
        return space.count_active()

    def part2(self, puzzle_input: list[str], part1_answer: int = 0) -> int:
        lines = puzzle_input
        rows = len(lines)
        columns = len(lines[0])
        space = Space4D(1, 1, rows, columns)
        for y, line in enumerate(lines):
            for x, cell in enumerate(line):
                space[(0, 0, y, x)] = (cell == '#')

        for i in range(0, 6):
            space.process_cycle()
        return space.count_active()

if __name__ == "__main__":
    Q17().solve()
