import io
import itertools
import math
from typing import Generator, Iterable

from util import AOCQuestion


class Q10(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> Generator[int, None, None]:
        return self.get_input_as_ints(stream)

    @staticmethod
    def find_joltage_differences(joltages: list[int]) -> dict[int, int]:
        joltages.append(0)
        joltages.append(max(joltages) + 3)
        adapters = sorted(joltages)
        joltage_diff = [j - i for i, j in zip(adapters[:-1], adapters[1:])]
        return {
            1: joltage_diff.count(1),
            2: joltage_diff.count(2),
            3: joltage_diff.count(3),
        }

    @staticmethod
    def find_adapter_chain_combinations(joltages: list[int]) -> int:
        joltages.append(0)
        joltages.append(max(joltages) + 3)
        adapters = sorted(joltages)
        joltage_diff = [j - i for i, j in zip(adapters[:-1], adapters[1:])]
        groups = [(i, sum(1 for _ in group)) for i, group in itertools.groupby(joltage_diff)]

        # For each run off 1's we need the combinations of 1's 2's and 3's that can make up that run.
        # 1,1     -> 2 (2)
        # 1,1,1   -> 1,2    2,1,   3 (4)
        # 1,1,1,1 -> 1,1,2  1,2,1  2,1,1,  2,2,  3,1,  1,3 (7)
        lookup = {
            1: 1,
            2: 2,
            3: 4,
            4: 7,
        }
        print

        return math.prod(lookup[i[1]] for i in groups if i[0] == 1)

    def part1(self, puzzle_input: Iterable[int]) -> int:
        joltage_diff = self.find_joltage_differences(list(puzzle_input))
        return joltage_diff[1] * joltage_diff[3]

    def part2(self, puzzle_input: Iterable[int], part1_answer: int = 0) -> int:
        return self.find_adapter_chain_combinations(list(puzzle_input))


if __name__ == "__main__":
    Q10().solve()
