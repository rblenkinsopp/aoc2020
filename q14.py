import io
from typing import Generator, Union

from util import AOCQuestion


class Q14(AOCQuestion):
    class Bitmask:
        def __init__(self):
            self.bits = {}

        def update(self, bitmask: str):
            self.bits.clear()
            for i, x in enumerate(reversed(bitmask)):
                if x == '1':
                    self.bits[i] = True
                elif x == '0':
                    self.bits[i] = False
                elif x == 'X':
                    self.bits[i] = None

        def apply(self, x: int) -> int:
            for i, bit in self.bits.items():
                if bit is None:
                    continue
                if bit:
                    x |= (1 << i)
                else:
                    x &= ~(1 << i)
            return x

        def apply_v2(self, x: int) -> set[int]:
            values = set()
            for i, bit in self.bits.items():
                if bit:
                    x |= (1 << i)
            values.add(x)
            for i, bit in self.bits.items():
                if bit is None:
                    new_values = set()
                    for value in values:
                        new_values.add(value | (1 << i))
                        new_values.add(value & ~(1 << i))
                    values = values.union(new_values)
            return values

    def parse_input(self, stream: io.TextIOBase) -> Generator[Union[str, tuple[int, int]], None, None]:
        for line in self.get_input_by_line(stream):
            if line.startswith('mask'):
                bitmask = line.split(' = ')[1]
                yield bitmask
            elif line.startswith('mem'):
                parts = line.split(' = ')
                address = int(parts[0].lstrip("mem[").rstrip("]"))
                value = int(parts[1])
                yield address, value

    @staticmethod
    def run_decoder(program: Generator[Union[str, tuple[int, int]], None, None], v2: bool = False) -> int:
        bitmask = Q14.Bitmask()
        mem: dict[int, int] = {}
        for instruction in program:
            if isinstance(instruction, str):
                bitmask.update(instruction)
            else:
                address, value = instruction
                if v2:
                    addresses = bitmask.apply_v2(address)
                    for new_address in addresses:
                        mem[new_address] = value
                else:
                    address, value = instruction
                    mem[address] = bitmask.apply(value)
        return sum(mem.values())

    def part1(self, puzzle_input: Generator[Union[str, tuple[int, int]], None, None]) -> int:
        return self.run_decoder(puzzle_input, False)

    def part2(self, puzzle_input: Generator[Union[str, tuple[int, int]], None, None], part1_answer: int = 0) -> int:
        return self.run_decoder(puzzle_input, True)


if __name__ == "__main__":
    Q14().solve()
