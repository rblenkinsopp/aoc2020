import io
import math

from util import AOCQuestion


class Q13(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> tuple[int, list[int]]:
        data = list(self.get_input_by_line(stream))
        timestamp = int(data[0])
        bus_ids = [int(x) if x != 'x' else None for x in data[1].split(',')]
        return timestamp, bus_ids

    def part1(self, puzzle_input: tuple[int, list[int]]) -> int:
        timestamp, bus_ids = puzzle_input
        minutes_to_bus = sorted([(bus_id, bus_id - (timestamp % bus_id)) for bus_id in bus_ids if bus_id is not None], key=lambda x: x[1])
        return math.prod(minutes_to_bus[0])

    def part2(self, puzzle_input: tuple[int, list[int]], part1_answer: int = 0) -> int:
        _, bus_ids = puzzle_input
        pertinent_ids = [(i, bus_id) for i, bus_id in enumerate(bus_ids) if bus_id is not None]

        # Number must be a multiple of the first id.
        timestamp = 0
        step = pertinent_ids[0][1]

        # Must also be a multiple when two ids are satisfied, so we can increase the step accordingly.
        for i, bus_id in pertinent_ids[1:]:
            while (timestamp + i) % bus_id != 0:
                timestamp += step
            step *= bus_id
        return timestamp


if __name__ == "__main__":
    Q13().solve()
