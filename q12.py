import io

from util import AOCQuestion


class Q12(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> list[tuple[str, int]]:
        return [(x[0], int(x[1:])) for x in self.get_input_by_line(stream)]

    def part1(self, puzzle_input: list[tuple[str, int]]) -> int:
        compass = ['N', 'E', 'S', 'W']
        needle = 1  # Start Facing East
        position = [0, 0]

        for direction, dist in puzzle_input:
            if direction == 'N':
                position[1] += dist
            elif direction == 'E':
                position[0] += dist
            elif direction == 'S':
                position[1] -= dist
            elif direction == 'W':
                position[0] -= dist
            elif direction == 'L':
                right_angles = dist // 90
                needle = ((needle - right_angles) % 4)
            elif direction == 'R':
                right_angles = dist // 90
                needle = ((needle + right_angles) % 4)
            elif direction == 'F':
                if compass[needle] == 'N':
                    position[1] += dist
                elif compass[needle] == 'E':
                    position[0] += dist
                elif compass[needle] == 'S':
                    position[1] -= dist
                elif compass[needle] == 'W':
                    position[0] -= dist
        return sum(abs(x) for x in position)

    def part2(self, puzzle_input, part1_answer: int = 0) -> int:
        waypoint_position = [10, 1]
        ship_position = [0, 0]

        for direction, dist in puzzle_input:
            if direction == 'N':
                waypoint_position[1] += dist
            elif direction == 'E':
                waypoint_position[0] += dist
            elif direction == 'S':
                waypoint_position[1] -= dist
            elif direction == 'W':
                waypoint_position[0] -= dist
            elif direction == 'L':
                right_angles = dist // 90
                for i in range(0, right_angles):
                    waypoint_position = [-waypoint_position[1], waypoint_position[0]]
            elif direction == 'R':
                right_angles = dist // 90
                for i in range(0, right_angles):
                    waypoint_position = [waypoint_position[1], -waypoint_position[0]]
            elif direction == 'F':
                ship_position = [ship_position[0] + waypoint_position[0] * dist, ship_position[1] + waypoint_position[1] * dist]
        return sum(abs(x) for x in ship_position)


if __name__ == "__main__":
    Q12().solve()
