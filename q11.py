import io
from copy import deepcopy

from util import AOCQuestion


class Q11(AOCQuestion):

    def parse_input(self, stream: io.TextIOBase) -> list[list[str]]:
        return list([list(line) for line in self.get_input_by_line(stream)])

    @staticmethod
    def get_adjacent_seats(seat_map: list[list[str]], x: int, y: int) -> list[str]:
        rows = len(seat_map)
        columns = len(seat_map[0])

        # ABC
        # D.E
        # FGH

        adjacent = []
        if y > 0 and x > 0:  # A
            adjacent.append(seat_map[y - 1][x - 1])
        if y > 0:  # B
            adjacent.append(seat_map[y - 1][x])
        if y > 0 and x < columns - 1:  # C
            adjacent.append(seat_map[y - 1][x + 1])
        if x > 0:  # D
            adjacent.append(seat_map[y][x - 1])
        if x < columns - 1:  # E
            adjacent.append(seat_map[y][x + 1])
        if y < rows - 1 and x > 0:  # F
            adjacent.append(seat_map[y + 1][x - 1])
        if y < rows - 1:  # G
            adjacent.append(seat_map[y + 1][x])
        if y < rows - 1 and x < columns - 1:  # H
            adjacent.append(seat_map[y + 1][x + 1])
        return adjacent

    @staticmethod
    def seat_cast(seat_map: list[list[str]], x: int, y: int, x_mod: int, y_mod: int):
        rows = len(seat_map)
        columns = len(seat_map[0])
        x += x_mod
        y += y_mod
        while (0 <= x < columns) and (0 <= y < rows):
            if seat_map[y][x] != '.':
                return seat_map[y][x]
            x += x_mod
            y += y_mod
        return None

    @staticmethod
    def get_visible_seats(seat_map: list[list[str]], x: int, y: int) -> list[str]:
        return [
            Q11.seat_cast(seat_map, x, y, -1, -1),
            Q11.seat_cast(seat_map, x, y, -1, 0),
            Q11.seat_cast(seat_map, x, y, -1, +1),
            Q11.seat_cast(seat_map, x, y, 0, -1),
            Q11.seat_cast(seat_map, x, y, 0, +1),
            Q11.seat_cast(seat_map, x, y, +1, -1),
            Q11.seat_cast(seat_map, x, y, +1, 0),
            Q11.seat_cast(seat_map, x, y, +1, +1)
        ]


    def part1(self, puzzle_input) -> int:
        rows = len(puzzle_input)
        columns = len(puzzle_input[0])

        changes = True
        original = puzzle_input
        update = deepcopy(original)

        while changes:
            original = deepcopy(update)
            changes = False
            for x in range(0, rows):
                for y in range(0, columns):
                    if original[y][x] == '.':  # Floor
                        continue
                    elif original[y][x] == '#':  # Occupied
                        # if four or more seats adjacent are occupied -> L
                        if self.get_adjacent_seats(original, x, y).count('#') >= 4:
                            update[y][x] = 'L'
                            changes = True
                    elif original[y][x] == 'L':
                        if self.get_adjacent_seats(original, x, y).count('#') == 0:
                            update[y][x] = '#'
                            changes = True
        return sum([row.count('#') for row in update])

    def part2(self, puzzle_input, part1_answer: int = 0) -> int:
        rows = len(puzzle_input)
        columns = len(puzzle_input[0])

        changes = True
        original = puzzle_input
        update = deepcopy(original)

        while changes:
            original = deepcopy(update)
            changes = False
            for x in range(0, rows):
                for y in range(0, columns):
                    if original[y][x] == '.':  # Floor
                        continue
                    elif original[y][x] == '#':  # Occupied
                        # if four or more seats adjacent are occupied -> L
                        if self.get_visible_seats(original, x, y).count('#') >= 5:
                            update[y][x] = 'L'
                            changes = True
                    elif original[y][x] == 'L':
                        if self.get_visible_seats(original, x, y).count('#') == 0:
                            update[y][x] = '#'
                            changes = True
        return sum([row.count('#') for row in update])


if __name__ == "__main__":
    Q11().solve()
