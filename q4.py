import io
import re
from typing import Generator

from util import AOCQuestion


def validate_iyr(entry: str) -> bool:
    return validate_year(2010, 2020, entry)


def validate_byr(entry: str) -> bool:
    return validate_year(1920, 2002, entry)


def validate_year(min_year: int, max_year: int, entry: str) -> bool:
    try:
        return min_year <= int(entry) <= max_year
    except ValueError:
        return False


def validate_eyr(entry: str) -> bool:
    return validate_year(2020, 2030, entry)


def validate_hgt(entry: str):
    regex = re.compile(r"^(\d+)(in|cm)$")
    matches = regex.match(entry)
    if matches:
        if matches[2] == "cm":
            return 150 <= int(matches[1]) <= 193
        elif matches[2] == "in":
            return 59 <= int(matches[1]) <= 76
    return False


def validate_hcl(entry: str) -> bool:
    return re.match(r"^#[a-f0-9]{6}$", entry) is not None


def validate_ecl(entry: str) -> bool:
    return re.match(r"^(?:amb|blu|brn|gry|grn|hzl|oth)$", entry) is not None


def validate_pid(entry: str) -> bool:
    return re.match(r"^\d{9}$", entry) is not None


class Q4(AOCQuestion):
    required_fields = {
        'byr': validate_byr,  # Birth Year
        'iyr': validate_iyr,  # Issue Year
        'eyr': validate_eyr,  # ExpirationYear
        'hgt': validate_hgt,  # Height
        'hcl': validate_hcl,  # HairColor
        'ecl': validate_ecl,  # Eye Color
        'pid': validate_pid,  # Passport ID
    }
    optional_fields = [
        'cid',  # Country ID
    ]

    def parse_input(self, stream: io.TextIOBase) -> Generator[dict[str, str], None, None]:
        entry = {}

        for line in self.get_input_by_line(stream):
            if line == "":
                yield entry
                entry.clear()
            else:
                for parameter in line.split(' '):
                    parts = parameter.split(':')
                    entry[parts[0]] = parts[1]
        yield entry

    def does_passport_have_all_required_entries(self, passport: dict[str, str]) -> bool:
        return all([x in passport.keys() for x in self.required_fields.keys()])

    def is_passport_valid(self, passport: dict[str, str]) -> bool:
        return all([x in passport.keys() and self.required_fields[x](passport[x]) for x in self.required_fields.keys()])

    def part1(self, puzzle_input) -> int:
        return sum([self.does_passport_have_all_required_entries(p) for p in puzzle_input])

    def part2(self, puzzle_input, part1_answer: int = 0) -> int:
        return sum([self.is_passport_valid(p) for p in puzzle_input])


if __name__ == "__main__":
    Q4().solve()
